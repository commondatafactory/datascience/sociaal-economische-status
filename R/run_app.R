#' run app
#'
#' @param ...
#'
#' @return Nothing
#'
#' @import shiny
#' @importFrom magrittr %>%
#'
#' @export
#'
#' @examples
#' \dontrun{
#' run_app()
#' }
run_app <- function(...) {

  # load the data
  df <- create_ses_woa_table(save_result=FALSE)

  # find all gemeenten
  gemeenten <- df %>%
    dplyr::filter(regionaal_niveau=="gemeente") %>%
    dplyr::select(WijkenEnBuurten, WijkenEnBuurten_label) %>%
    dplyr::distinct()

  # modify some variables
  df <- df %>%
    dplyr::mutate(
      Jaar = as.integer(as.character(Perioden_label))
    )

  # See above for the definitions of ui and server
  ui <- fluidPage(

    # App title ----
    titlePanel("SES-WOA"),

    # Sidebar layout with input and output definitions ----
    sidebarLayout(

      # Sidebar panel for inputs ----
      sidebarPanel(

        # Input: Dropdown for gemeente
        shinyWidgets::pickerInput(
          "gemeentecode",
          label = "Selecteer gemeente(n)",
          choices = setNames(
            gemeenten$WijkenEnBuurten,
            gemeenten$WijkenEnBuurten_label
          ),
          selected = NULL,
          multiple = TRUE,
          options = list(
            `live-search` = TRUE,
            `live-search-normalize` = TRUE,
            `live-search-style` = 'contains',
            `select-on-tab` = TRUE,
            title = "Kies een gemeente"
          )
        ),


        # Input: Dropdown for wijk
        uiOutput(outputId = "select_wijk")

      ),

      # Main panel for displaying outputs ----
      mainPanel(

        # Output: plot of trend over time
        plotOutput(outputId = "trendGemeente"),

        # Output: plot of trend over time
        plotOutput(outputId = "trendWijk")

      )
    )
  )

  server <- function(input, output) {

    wijken <- reactive({
      req(input$gemeentecode)
      out <- df %>%
        dplyr::filter(regionaal_niveau=="wijk") %>%
        dplyr::filter(
          startsWith(
            WijkenEnBuurten,
            sub("GM", "WK", input$gemeentecode)
          )
        ) %>%
        dplyr::select(WijkenEnBuurten, WijkenEnBuurten_label) %>%
        dplyr::distinct()

      return(out)
    })

    output$select_wijk <- renderUI({

      shinyWidgets::pickerInput(
        "wijkcode",
        label = "Selecteer wijk(en)",
        choices = setNames(
          wijken()$WijkenEnBuurten,
          wijken()$WijkenEnBuurten_label
        ),
        selected = NULL,
        multiple = TRUE,
        options = list(
          `live-search` = TRUE,
          `live-search-normalize` = TRUE,
          `live-search-style` = 'contains',
          `select-on-tab` = TRUE,
          title = "Kies een wijk"
        )
      )
    })

    output$trendGemeente <- renderPlot({

      ggplot2::ggplot(
        data = df %>%
          dplyr::filter(WijkenEnBuurten %in% input$gemeentecode),
        mapping = ggplot2::aes(
          x=Jaar,
          y=GemiddeldeScore_29,
          color = WijkenEnBuurten_label
        )
      ) +
        ggplot2::geom_line(size=2) +
        ggplot2::geom_point(size=2) +
        ggplot2::ggtitle("Gemeente") +
        ggplot2::ylab("SES-WOA") +
        ggplot2::theme(text = ggplot2::element_text(size = 16))
    })

    output$trendWijk <- renderPlot({

      ggplot2::ggplot(
        data = df %>%
          dplyr::filter(WijkenEnBuurten %in% input$wijkcode),
        mapping = ggplot2::aes(
          x=Jaar,
          y=GemiddeldeScore_29,
          color = WijkenEnBuurten_label
        )
      ) +
        ggplot2::geom_line(size=2) +
        ggplot2::geom_point(size=2) +
        ggplot2::ggtitle("Wijk") +
        ggplot2::ylab("SES-WOA") +
        ggplot2::theme(text = ggplot2::element_text(size = 16))
    })
  }

  shinyApp(ui = ui, server = server)
}
