uitprobeersel <- function() {
  # bereken de sociaal economische status

  # laad de data van CBS
  "
* het gemiddelde inkomen in een wijk,
  - per postcode: https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/gegevens-per-postcode
  - per buurt/wijk: https://www.cbs.nl/nl-nl/maatwerk/2019/31/kerncijfers-wijken-en-buurten-2019
* het percentage mensen met een laag inkomen,
  - per postcode: https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/gegevens-per-postcode
  - per buurt/wijk: https://www.cbs.nl/nl-nl/maatwerk/2019/31/kerncijfers-wijken-en-buurten-2019
* het percentage laag opgeleiden,
  - per wijk: https://opendata.cbs.nl/#/CBS/nl/dataset/84773NED/table
  - per gemeente: https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84961NED/table?dl=50612
* het percentage mensen dat niet werkt.
  - per gemeente: https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84961NED/table?dl=50612
  - per buurt/wijk: netto arbeidsparticipatie

"

  # oude ses 1998, 2002, 2006, 2010, 2014, 2016, 2017

  # op postcode:
  # laag opgeleid: 2014
  # gemiddeld inkomen: 2014
  # werklozen: 2015, 2016
  # laag inkomen: 2015, 2016

  # opleiding en netto arbeidsparticipatie vanaf 2019 in kerncijfers aanwezig
  # maar inkomen mist voor veel wijken.
  # daarom inschatting op gemeenteniveau

  library(cbsodataR)
  library(dplyr)

  library(readxl)

  df.opleiding <- read_excel("data/opleidingsniveau-vso.xlsx", "Tabel", skip=1)



  df.raw <- read_excel("data/kwb-2017.xls")

  df <- df.raw %>%
    mutate(
      gemeentenaam=gm_naam,
      naam=regio,
      aantal_inwoners=a_inw,
      gemiddeld_inkomen=g_ink_pi,
      percentage_laag_inkomen=p_ink_li,
      aantal_laag_opgeleid=a_opl_lg,
      percentage_werken=p_arb_pp
    ) %>%
    select(
      gwb_code,
      gemeentenaam,
      naam,
      aantal_inwoners,
      gemiddeld_inkomen,
      percentage_laag_inkomen,
      aantal_laag_opgeleid,
      percentage_werken
    )

  "
Conclusies:
1. De relevante informatie is niet op 4-cijferige postcode informatie beschikbaar in open datasets
2. Vanaf 2019 is de informatie opgenomen in de kerncijfers buurt/wijk/gemeente
3. De oude ses is tot 2017 bekend
4. Eventueel zou de ses mogelijk op gemeenteniveau voor eerdere jaren berekend kunnen worden.
5. De ses is het meest relevant op wijkniveau.
6. In de wijkdata van 2021 en 2020 zijn de relevante variabelen allemaal NA

Acties:
1. Bereken de ses eerst voor 2019 en kijk hoe dat eruit ziet.
"

  # laad de data
  df.raw <- cbs_get_data(
    id="84583NED",
    WijkenEnBuurten = has_substring("GM")
  )

  # selecteer de relevante variabelen
  df <- df.raw %>%
    mutate(
      gwbcode = WijkenEnBuurten,
      gemeentenaam = Gemeentenaam_1,
      aantal_inwoners = AantalInwoners_5,
      gemiddeld_inkomen = GemiddeldInkomenPerInwoner_72,
      percentage_laag_inkomen = k_40PersonenMetLaagsteInkomen_73,
      percentage_laag_opgeleid = 100*(OpleidingsniveauLaag_64 / aantal_inwoners),
      percentage_werklozen = 100 - Nettoarbeidsparticipatie_67
    ) %>%
    select(
      gwbcode,
      gemeentenaam,
      aantal_inwoners,
      gemiddeld_inkomen,
      percentage_laag_inkomen,
      percentage_laag_opgeleid,
      percentage_werklozen
    ) %>%
    na.omit

  # principal components
  pZ <- prcomp(
    ~ gemiddeld_inkomen + percentage_laag_inkomen + percentage_laag_opgeleid + percentage_werklozen,
    data = df
  )

  # voeg de ses toe
  df <- cbind(df, predict(pZ))

  # zet op volgorde
  df <- df %>%
    mutate(ses=scale(PC1)) %>%
    arrange(ses)

}
