
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Sociaal Economische Status

<!-- badges: start -->
<!-- badges: end -->

Het doel van dit R-pakket is om de nieuwe manier van de berekening van
de SES (Sociaal Economische Status) te vergelijken met de oude methode.
De oude methode werd gepubliceerd door het centraal planbureau en was
gebaseerd op vragenlijstuitkomsten rondom scholing, inkomen en werk. De
nieuwe methode levert de SES-WOA en is gebaseerd op CBS data rondom
scholing, inkomen en werk.

## Installatie

Het pakket kan worden geïnstalleerd als volgt:

``` r
install.packages("devtools")
devtools::install_gitlab("https://gitlab.com/commondatafactory/datascience/sociaal-economische-status")
```

## Voorbeeld

De nieuwe SES-WOA kan worden gevisualiseerd als volgt:

``` r
library(ses)
run_app()
```

## To do

In de folder `src` staat een functie die de oude SES met de nieuwe
SES-WOA vergelijkt. Deze functie moet nog worden aangepast zodat deze de
juiste bestanden van de oude SES kan downloaden en het
grenswijzigingen-pakket kan gebruiken.

-   Toevoegen oude SES-data bestanden
-   Toevoegen inlezen oude SES-data bestanden
-   Toevoegen grenswijzigen van de oude SES

## Licentie

<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dit
werk valt onder een
<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl">Creative
Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0
Internationaal-licentie</a>.
